// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpeed.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AFoodSpeed::AFoodSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval(Snake->MovementSpeed * 0.8f);

			int RandomX = FMath::RandRange(-750, 750);
			int RandomY = FMath::RandRange(-1350, 1350);
			FVector NewLocation = FVector(RandomX, RandomY, 20);

			SetActorLocation(NewLocation);
		}
	}
}

