// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGame_1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_1_API ASnakeGame_1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};