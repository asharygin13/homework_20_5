// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			
			bool bInSnake = true;
			FVector NewLocation;

			const float MinDistanceThreshold = 100.0f;

			while (bInSnake) {
				int RandomX = FMath::RandRange(-750, 750);
				int RandomY = FMath::RandRange(-1350, 1350);
				NewLocation = FVector(RandomX, RandomY, 20);

				bInSnake = false;

				// �������� ��� ������� �������� ������
				for (auto SnakeElement : Snake->SnakeElements) 
				{
					FVector SnakeElementLocation = SnakeElement->GetActorLocation();
					if (FVector::Dist(NewLocation, SnakeElementLocation) < MinDistanceThreshold) 
					{
						bInSnake = true;
						break;
					}
				}
			}

			//Destroy();

			//int RandomX = FMath::RandRange(-750, 750);
			//int RandomY = FMath::RandRange(-1350, 1350);
			//FVector NewLocation = FVector(RandomX, RandomY, 20);

			FTimerHandle TimerHandle;

			// ������ ���������� ������� �� 20 ������
			GetWorldTimerManager().SetTimer(TimerHandle, this, &AFood::FoodExpiration, 20.0f, false);

			SetActorLocation(NewLocation);
		}
	}
}

void AFood::FoodExpiration()
{
	auto Snake = Cast<ASnakeBase>(GetOwner());

	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

